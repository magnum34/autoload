<?php

function autoload($class) {
    echo 'Ładuję klasę '.$class."<br>"; 
    //ścieżka która bedzie ładowany cały projekt lub biblioteka, framework
    $file = "./dir/".$class. '.php';
    if(file_exists($file)){
        require_once $file;
        return;
    }
    throw new Exception("Nie znaleziono klasy {$class}");   
}
//zapisanie funkcji jako autoloader ( leniwe ładowanie)
spl_autoload_register('autoload');

$hello = new Hello();
$hello->world = "foo!";
echo $hello->world;

?>
