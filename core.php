<?php



/**
 * Description of core
 *
 * @author magnum
 */
//automatyczne ładowanie w klasie
namespace AutoLoad{
    class Core {

        public static function initialize(){
            //Załadowanie funkcji _autoload
            spl_autoload_register(__CLASS__."::_autoload");
        }
        protected static function _autoload($class)
        {
            $paths = explode(PATH_SEPARATOR, get_include_path());
            $flags = PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE;
            $file = strtolower(str_replace("\\", DIRECTORY_SEPARATOR, trim($class, "\\"))).".php";
            foreach ($paths as $path)
            {
                $combined = $path.DIRECTORY_SEPARATOR.$file;

                if (file_exists($combined))
                {
                    
                    include($combined);
                    return;
                }
            }
            
            throw new Exception("{$class} not found");
        }
    }
    
    
}

?>